<?php
namespace App\Service;

use App\User;

class UserLdapToJson {

    public function run($user = null)
    {
        return [
            "displayName" => $user["displayName"][0],
            "filial" => $user["physicaldeliveryofficename"][0],
            "matricula" => $user["facsimiletelephonenumber"][0],
            "sAMAccountName" => $user["sAMAccountName"][0],
            "userPrincipalName" => $user["userPrincipalName"][0]
        ];
    }

}