<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Adldap\Laravel\Facades\Adldap;
use App\Service\UserLdapToJson;

class WsldapController extends Controller
{
    public function authenticate(Request $request) {
        
        $username = $request->input('user');
        $password = $request->input('password');

        if( is_null($username) || is_null($password) ) {
            return response()->json(['message' => 'Paramentros incorretos']);
        }
           
        $bind = Adldap::auth()->attempt($username, $password);

        if($bind === true) {
            $search = Adldap::search()->where('samaccountname', '=', $username)->get();
            
            if(count($search)) {
                $json = (new UserLdapToJson())->run($search[0]); 
                $json['sucesso'] = true;
                return response()->json($json);
            }
        }

        return response()->json(["sucesso" => false, "message" => "usuário inválido"], 404);
    }

    public function getUserByUsername($username)
    {
        $search = Adldap::search()->where('samaccountname', '=', $username)->get();

        if(count($search)) {
            $json = (new UserLdapToJson())->run($search[0]); 
            return response()->json($json);
        }

        return response()->json(["message" => "Usuário não encontrado"], 404);
    }

    public function getUserByEm($em)
    {
        $search = Adldap::search()->where('facsimiletelephonenumber', '=', "EM".$em)->get();

        if(count($search)) {
            $json = (new UserLdapToJson())->run($search[0]); 
            return response()->json($json);
        }

        return response()->json(["message" => "Usuário não encontrado"], 404);
    }

    
}
